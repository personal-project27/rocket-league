package com.rocket.league.bot.component.handler.specific;

import static basic.util.menu.model.response.MenuResponseStatus.OK;

import java.util.Map;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;

import com.rocket.league.bot.component.user.model.UserDTO;
import com.rocket.league.bot.component.user.service.UserService;
import com.rocket.league.bot.component.general.custombutton.service.CustomButtonService;

import basic.util.handler.specific.AbstractSpecificHandler;
import basic.util.menu.HandlerMenu;
import basic.util.user.action.ActionService;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
public class HandlerRegistered extends AbstractSpecificHandler<UserDTO> {
	private final UserService userService;

	public HandlerRegistered(final HandlerMenu handlerMenu, final ActionService actionService,
			final UserService userService, final CustomButtonService customButtonService) {
		super(handlerMenu, actionService, customButtonService);
		this.userService = userService;
	}

	@Override
	public Map<String, Function<UserDTO, Mono<EditMessageText>>> getStates() {
		return Map
				.of( //
						"REGISTERED", this::mainMenu, //
						"REGISTER_PLATFORM", u -> createEditMessage(u, false, OK), //
						"REGISTER_PLATUSER", this::registerPlatUser, //
						"SAVE_USER", this::saveUser);
	}

	@Override
	public Map<String, Function<UserDTO, Mono<Tuple2<AnswerCallbackQuery, EditMessageText>>>> getPopup() {
		return Map.of();
	}

	private Mono<EditMessageText> mainMenu(final UserDTO u) {
		boolean isNewUser = StringUtils.isEmpty(u.getPlatForm());

		return createEditMessage(u, !isNewUser, OK);
	}

	private Mono<EditMessageText> registerPlatUser(final UserDTO u) {
		final String platForm = actionService
				.getLastActionElement(u.getAction(), f -> f.getActionButtonData().getCallKey());

		return createEditMessage(u, false, OK, platForm);
	}

	private Mono<EditMessageText> saveUser(final UserDTO u) {
		return userService
				.addAccount(u)
				.flatMap(newU -> userService.newState(newU, "REGISTERED"))
				.flatMap(this::mainMenu);
	}
}
