package com.rocket.league.bot.component.general.custombutton.repository;

import org.com.bot.model.custombutton.CustomButton;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CustomButtonRepository extends ReactiveMongoRepository<CustomButton, String> {
    Mono<CustomButton> findByNameAndChatId(final String name, final Long chatId);
    Mono<Void> deleteByNameAndChatId(final String name, final Long chatId);
}
