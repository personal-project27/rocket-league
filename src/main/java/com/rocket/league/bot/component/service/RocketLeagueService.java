package com.rocket.league.bot.component.service;

import com.rocket.league.bot.component.user.model.UserDTO;
import com.rocket.league.model.scraping.MyData;

import reactor.core.publisher.Mono;

public interface RocketLeagueService {

	Mono<MyData> retrieveStats(final UserDTO u);
}
