package com.rocket.league.bot.component.general;

import static io.vavr.API.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.function.Predicate;

import org.telegram.telegrambots.meta.api.objects.Update;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rocket.league.model.scraping.MyDataSteam;

import basic.util.other.interfaces.IBotUtils;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
public class BasicService implements IBotUtils {
	public static final Predicate<Update> checkCallback = Update::hasCallbackQuery;
	public static final Predicate<Update> checkMessage = Update::hasMessage;
	public static final Predicate<Update> checkEditedMessage = Update::hasEditedMessage;

	private static final Predicate<MyDataSteam> isDataSteamNotEmpty = o -> !o.getData().isEmpty();
	private static final Predicate<String> isSteamOrEpic = s -> List.of("steam", "epic").contains(s.toLowerCase());
	private static final Predicate<Boolean> is200 = b -> Boolean.compare(true, b) == 0;

	public Mono<URL> callApi(final String url) {
		return Mono
				.just(url)
				.flatMap(this::createURL)
				.flatMap(this::checkAPIResult)
				.filter(is200)
				.flatMap(b -> this.createURL(url))
				.switchIfEmpty(Mono.empty());
	}

	public <T> Mono<T> createData(final URL url, final Class<T> c) {
		return Mono
				.justOrEmpty(url)
				.map(u -> Try
						.of(() -> new ObjectMapper()
								.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
								.readValue(u, c))
						.getOrElse(Try.of(() -> c.getDeclaredConstructor().newInstance()).get()))
				.switchIfEmpty(Mono.just(Try.of(() -> c.getDeclaredConstructor().newInstance()).get()));
	}

	private Mono<URL> createURL(final String url) {
		return Mono.just(url).map(u -> Try.of(() -> new URL(u)).onFailure(Throwable::printStackTrace).get());
	}

	private Mono<Boolean> checkAPIResult(final URL url) {
		return Mono
				.justOrEmpty(url)
				.map(u -> Try
						.of(() -> ((HttpURLConnection) u.openConnection()).getResponseCode())
						.onFailure(Throwable::printStackTrace)
						.get())
				.map(h -> Match(h).of(Case($(200), f -> true), Case($(), f -> false)))
				.switchIfEmpty(Mono.just(false));
	}

}
