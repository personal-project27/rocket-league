package com.rocket.league.bot.component.handler;

import basic.util.AbstractHandler;
import basic.util.config.AppBotConfig;
import basic.util.other.LoggingComponent;
import com.rocket.league.bot.component.user.model.UserDTO;
import com.rocket.league.bot.component.user.service.UserService;
import com.rocket.league.bot.component.general.messages.service.MessageService;
import com.rocket.league.model.user.User;
import org.springframework.stereotype.Component;

@Component
public class Handler extends AbstractHandler<UserDTO, User> {
    public Handler(final UserService userService, final HandlerService handlerService, final AppBotConfig appBotConfig,
                   final MessageService messageService, final LoggingComponent log) {
        super(userService, handlerService, appBotConfig, messageService, log);
    }

}