package com.rocket.league.bot.component.user.transformer;

import java.time.LocalDateTime;
import java.util.List;

import org.com.bot.model.action.ActionBasic;
import org.com.bot.model.messages.UserMessages;
import org.com.bot.model.state.State;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

import com.rocket.league.bot.component.user.model.UserDTO;
import com.rocket.league.model.rank.Rank;
import com.rocket.league.model.stats.StatsList;
import com.rocket.league.model.user.User;

import basic.util.user.transformer.IUserTransformer;
import reactor.core.publisher.Mono;

@Component
public class UserTransformer implements IUserTransformer<UserDTO, User> {
	@Override
	public Mono<UserDTO> transformFromPersistence(final User u) {
		return Mono
				.just(UserDTO
						.builder()
						.chatId(u.getChatId())
						.state(u.getState())
						.rankList(u.getRankList())
						.statsList(u.getStatList())
						.isAdmin(u.getIsAdmin())
						.action(u.getAction())
						.registerDate(u.getRegisterDate())
						.lastAccess(LocalDateTime.now())
						.platUser(u.getPlatUser())
						.platForm(u.getPlatForm())
						.build());
	}

	@Override
	public Mono<User> transformFromDTO(final UserDTO u) {
		return Mono
				.just(User
						.builder()
						.chatId(u.getChatId())
						.state(u.getState())
						.rankList(u.getRankList())
						.statList(u.getStatsList())
						.isAdmin(u.getIsAdmin())
						.action(u.getAction())
						.registerDate(u.getRegisterDate())
						.lastAccess(LocalDateTime.now())
						.platUser(u.getPlatUser())
						.platForm(u.getPlatForm())
						.build());
	}

	@Override
	public Mono<UserDTO> updateState(final UserDTO u, final State s) {
		return Mono
				.just(UserDTO
						.builder()
						.chatId(u.getChatId())
						.state(s)
						.isAdmin(u.getIsAdmin())
						.action(u.getAction())
						.registerDate(u.getRegisterDate())
						.lastAccess(LocalDateTime.now())
						.statsList(u.getStatsList())
						.rankList(u.getRankList())
						.update(u.getUpdate())
						.message(u.getMessage())
						.platUser(u.getPlatUser())
						.platForm(u.getPlatForm())
						.build());
	}

	@Override
	public Mono<UserDTO> addAction(final UserDTO u, final ActionBasic a) {
		return Mono
				.just(UserDTO
						.builder()
						.chatId(u.getChatId())
						.state(u.getState())
						.isAdmin(u.getIsAdmin())
						.action(a)
						.registerDate(u.getRegisterDate())
						.lastAccess(LocalDateTime.now())
						.statsList(u.getStatsList())
						.rankList(u.getRankList())
						.update(u.getUpdate())
						.message(u.getMessage())
						.platUser(u.getPlatUser())
						.platForm(u.getPlatForm())
						.build());
	}

	@Override
	public Mono<UserDTO> addUpdate(final UserDTO u, final Update upd) {
		return Mono
				.just(UserDTO
						.builder()
						.chatId(u.getChatId())
						.state(u.getState())
						.isAdmin(u.getIsAdmin())
						.action(u.getAction())
						.registerDate(u.getRegisterDate())
						.lastAccess(LocalDateTime.now())
						.statsList(u.getStatsList())
						.rankList(u.getRankList())
						.update(upd)
						.message(u.getMessage())
						.platUser(u.getPlatUser())
						.platForm(u.getPlatForm())
						.build());
	}

	@Override
	public Mono<UserDTO> addMessage(final UserDTO u, final UserMessages msg) {
		return Mono
				.just(UserDTO
						.builder()
						.chatId(u.getChatId())
						.state(u.getState())
						.isAdmin(u.getIsAdmin())
						.action(u.getAction())
						.registerDate(u.getRegisterDate())
						.lastAccess(LocalDateTime.now())
						.message(msg)
						.statsList(u.getStatsList())
						.rankList(u.getRankList())
						.update(u.getUpdate())
						.platUser(u.getPlatUser())
						.platForm(u.getPlatForm())
						.build());
	}

	public Mono<User> addAccount(final UserDTO u, final String platUser, final String platForm) {
		return Mono
				.just(User
						.builder()
						.chatId(u.getChatId())
						.state(u.getState())
						.isAdmin(u.getIsAdmin())
						.action(u.getAction())
						.registerDate(u.getRegisterDate())
						.lastAccess(LocalDateTime.now())
						.statList(u.getStatsList())
						.rankList(u.getRankList())
						.platUser(platUser)
						.platForm(platForm)
						.build());
	}

	public Mono<User> addStatsAndRank(final UserDTO u, final List<Rank> t1, final StatsList t2) {
		return Mono
				.just(User
						.builder()
						.chatId(u.getChatId())
						.state(u.getState())
						.isAdmin(u.getIsAdmin())
						.action(u.getAction())
						.registerDate(u.getRegisterDate())
						.lastAccess(LocalDateTime.now())
						.statList(t2)
						.rankList(t1)
						.platUser(u.getPlatUser())
						.platForm(u.getPlatForm())
						.build());
	}

}
