package com.rocket.league.bot.component.general.messages.repository;

import org.com.bot.model.messages.UserMessages;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends ReactiveMongoRepository<UserMessages, Long> {
}
