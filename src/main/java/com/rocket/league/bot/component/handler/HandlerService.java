package com.rocket.league.bot.component.handler;

import basic.util.handler.interfaces.IServiceHandler;
import basic.util.user.BotUser;
import com.rocket.league.bot.component.user.model.UserDTO;
import com.rocket.league.bot.component.handler.specific.HandlerProfile;
import com.rocket.league.bot.component.handler.specific.HandlerRegistered;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.List;

import static io.vavr.API.*;

@Service
@AllArgsConstructor
public class HandlerService implements IServiceHandler {
    private final HandlerRegistered handlerRegistered;
    private final HandlerProfile handlerProfile;

    @Override
    public Mono<Tuple2<AnswerCallbackQuery, EditMessageText>> matcherTuple(final BotUser botUser) {
        return Mono
                .just(botUser)
                .cast(UserDTO.class)
                .flatMap(c -> Match(c)
                        .of( //
                                Case($(handlerRegistered::isPopupOk), handlerRegistered::executePopup), //
                                Case($(handlerProfile::isPopupOk), handlerProfile::executePopup) //
                        ));
    }

    @Override
    public Mono<EditMessageText> matcherNormal(final BotUser botUser) {
        return Mono
                .just(botUser)
                .cast(UserDTO.class)
                .flatMap(u -> Match(u)
                        .of( //
                                Case($(handlerRegistered::isStateOk), handlerRegistered::executeState), //
                                Case($(handlerProfile::isStateOk), handlerProfile::executeState), //
                                Case($(), this::emptyMessage) //
                        ));
    }

    @Override
    public Mono<Tuple2<List<SendMessage>, EditMessageText>> matcherAdmin(final BotUser botUser) {
        return Mono
                .empty();
    }
}
