package com.rocket.league.bot.component.general.state.service;

import basic.util.config.AppBotConfig;
import basic.util.user.state.IStateService;
import basic.util.user.state.StateException;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.rocket.league.bot.component.general.state.repository.StateRepository;
import org.com.bot.model.state.State;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import reactor.cache.CacheMono;
import reactor.core.publisher.Mono;

import static java.util.concurrent.TimeUnit.DAYS;

@Service
public class StateService implements IStateService {
    private final StateRepository repository;
    private final LoadingCache<String, Object> cache;
    private final AppBotConfig appBotConfig;

    public StateService(final StateRepository repository, final AppBotConfig appBotConfig) {
        this.repository = repository;
        this.appBotConfig = appBotConfig;
        this.cache = Caffeine.newBuilder().expireAfterWrite(3, DAYS).build(this::getState);
    }

    @Override
    @Cacheable("state")
    public Mono<State> getState(final String id) {
        return CacheMono
                .lookup(cache.asMap(), id)
                .onCacheMissResume(() -> repository.findById(id).cast(Object.class))
                .cast(State.class)
                .switchIfEmpty(Mono.defer(() -> Mono.error(new StateException(id))));
    }

    public Mono<State> initialState(final Boolean isAdmin) {
        final Boolean isSeparate = Boolean.parseBoolean(appBotConfig.getIsSeparateView()) && isAdmin;

        return repository
                .findByIsInitialStateAndIsAdminState(true, isSeparate)
                .switchIfEmpty(Mono.defer(() -> Mono.error(new StateException("Iniziale"))));
    }
}
