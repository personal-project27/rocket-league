package com.rocket.league.bot.component.general.menu;

import basic.util.menu.HandlerMenu;
import basic.util.user.BotUser;
import basic.util.user.action.ActionService;
import com.rocket.league.bot.component.user.model.UserDTO;
import lombok.RequiredArgsConstructor;
import org.com.bot.model.menu.Button;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static io.vavr.API.*;

@Service
@RequiredArgsConstructor
public class SpecificHandlerMenu extends HandlerMenu {
    private final ActionService actionService;

    @Override
    public Boolean modifyVisibility(final Button m, final BotUser u) {
        final UserDTO user = (UserDTO) u;

        return Optional
                .ofNullable(m.getButtonDynamicText())
                .map(mc -> Match(mc.getPopupKey()).of(Case($(), f -> m.getIsVisible())))
                .orElseGet(m::getIsVisible);
    }

    @Override
    public String modifyText(final Button m, final BotUser u) {
        final UserDTO user = (UserDTO) u;

        return Optional
                .ofNullable(m.getButtonDynamicText())
                .map(mc -> Match(mc.getPopupKey())
                        .of(Case($(), f -> m.getText())))
                .orElse(m.getText());
    }

    @Override
    public String modifyData(final Button m, final BotUser user) {
        final UserDTO u = (UserDTO) user;

        return Optional
                .ofNullable(m.getButtonDynamicText())
                .map(mc -> Match(mc.getPopupKey())
                        .of(Case($(), f -> m.getData()) //
                        ))
                .orElse(m.getData());
    }
}
