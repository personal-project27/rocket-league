package com.rocket.league.bot.component.general.messages.service;

import basic.util.messages.IMessageService;
import com.rocket.league.bot.component.general.messages.repository.MessageRepository;
import lombok.RequiredArgsConstructor;
import org.com.bot.model.messages.UserMessages;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class MessageService implements IMessageService {
    private final MessageRepository repository;

    @Override
    public Mono<UserMessages> save(final UserMessages t) {
        return repository.save(t);
    }

    @Override
    public Mono<UserMessages> findById(final Long id) {
        return repository
                .findById(id)
                .switchIfEmpty(Mono.defer(() -> Mono.just(UserMessages.builder().chatId(id).build())));
    }
}
