package com.rocket.league.bot.component.user.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.rocket.league.bot.component.general.messages.service.MessageService;
import com.rocket.league.bot.component.service.RankService;
import com.rocket.league.bot.component.service.RocketLeagueServiceImpl;
import com.rocket.league.bot.component.service.StatsService;
import com.rocket.league.bot.component.general.state.service.StateService;
import com.rocket.league.bot.component.user.model.UserDTO;
import com.rocket.league.bot.component.user.repository.UserRepository;
import com.rocket.league.bot.component.user.transformer.UserTransformer;
import com.rocket.league.model.user.User;

import basic.util.user.AbstractBotService;
import basic.util.user.action.ActionService;
import reactor.core.publisher.Mono;

@Service
public class UserService extends AbstractBotService<UserDTO, User> {
	private final UserTransformer transformer;
	private final UserRepository repository;
	private final RocketLeagueServiceImpl rocketLeagueService;
	private final StatsService statsService;
	private final RankService rankService;

	public UserService(final StateService stateService, final ActionService actionService,
					   final MessageService messageService, final UserTransformer transformer, final UserRepository repository,
					   final RocketLeagueServiceImpl rlService, final StatsService statsService, final RankService rankService) {
		super(transformer, stateService, actionService, messageService, User.class);
		this.transformer = transformer;
		this.repository = repository;
		this.rocketLeagueService = rlService;
		this.statsService = statsService;
		this.rankService = rankService;
	}

	@Override
	public Mono<User> findById(final Long chatId) {
		return repository.findById(chatId);
	}

	@Override
	public Mono<User> save(final User user) {
		return repository.save(user);
	}

	@Override
	public Boolean isAdminCode(final Long chatId) {
		return List.of(182150319L, 181406924L).contains(chatId);
	}

	public Mono<UserDTO> addAccount(final UserDTO u) {
		final String platUser = actionService.getLastAction(u.getAction().getActions()).getTextWritten();
		final String platForm = actionService
				.getSpecificActionElement(u.getAction(), "REGISTER_PLATUSER",
						a -> a.getActionButtonData().getCallKey());

		return transformer
				.addAccount(u, platUser, platForm)
				.flatMap(repository::save)
				.flatMap(transformer::transformFromPersistence);
	}

	public Mono<UserDTO> updateProfile(final UserDTO u) {

		return rocketLeagueService
				.retrieveStats(u)
				.filter(l -> l.getData() != null)
				.flatMap(l -> Mono.zip(rankService.setRank(l), statsService.setStats(l)))
				.flatMap(tp -> transformer.addStatsAndRank(u, tp.getT1(), tp.getT2()))
				.flatMap(repository::save)
				.flatMap(transformer::transformFromPersistence)
				.switchIfEmpty(Mono.defer(Mono::empty));
	}

}
