package com.rocket.league.bot.component.handler.specific;

import static basic.util.menu.model.response.MenuResponseStatus.KO;
import static basic.util.menu.model.response.MenuResponseStatus.OK;

import java.util.Map;
import java.util.function.Function;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;

import com.rocket.league.bot.component.user.model.UserDTO;
import com.rocket.league.bot.component.user.service.UserService;
import com.rocket.league.bot.component.general.custombutton.service.CustomButtonService;

import basic.util.handler.specific.AbstractSpecificHandler;
import basic.util.menu.HandlerMenu;
import basic.util.user.action.ActionService;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
public class HandlerProfile extends AbstractSpecificHandler<UserDTO> {
	private final UserService userService;

	public HandlerProfile(final HandlerMenu handlerMenu, final ActionService actionService,
			final UserService userService, final CustomButtonService customButtonService) {
		super(handlerMenu, actionService, customButtonService);
		this.userService = userService;
	}

	@Override
	public Map<String, Function<UserDTO, Mono<EditMessageText>>> getStates() {
		return Map
				.of( //
						"PROFILE", this::profile //
				);
	}

	@Override
	public Map<String, Function<UserDTO, Mono<Tuple2<AnswerCallbackQuery, EditMessageText>>>> getPopup() {
		return Map.of();
	}

	private Mono<EditMessageText> profile(final UserDTO u) {

		return userService.updateProfile(u).flatMap(l -> createEditMessage(l, false, OK, u.getRankList(), u.getStatsList()))
				.switchIfEmpty(Mono.defer(() -> createEditMessage(u, false, KO)));
	}

}