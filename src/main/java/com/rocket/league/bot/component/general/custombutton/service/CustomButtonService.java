package com.rocket.league.bot.component.general.custombutton.service;

import basic.util.user.custombutton.AbstractCustomButtonService;
import com.rocket.league.bot.component.general.custombutton.repository.CustomButtonRepository;
import org.com.bot.model.custombutton.CustomButton;
import org.com.bot.model.menu.ButtonDynamicText;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class CustomButtonService extends AbstractCustomButtonService {
    private final CustomButtonRepository repository;

    public CustomButtonService(final CustomButtonRepository repository) {
        this.repository = repository;
    }

    @Override
    public Mono<CustomButton> save(final CustomButton button) {
        return repository.save(button);
    }

    @Override
    public Flux<CustomButton> saveAll(final List<CustomButton> list) {
        return repository.saveAll(list);
    }

    @Override
    public Mono<CustomButton> findByNameAndChatId(final String name, final Long chatId) {
        return repository.findByNameAndChatId(name, chatId);
    }

    @Override
    public Mono<Void> deleteByNameAndChatId(final String name, final Long chatId) {
        return repository.deleteByNameAndChatId(name, chatId);
    }
}
