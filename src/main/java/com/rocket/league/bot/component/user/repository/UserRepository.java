package com.rocket.league.bot.component.user.repository;

import com.rocket.league.model.user.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, Long> {
    Flux<User> findAllByChatIdIsNot(final Long chatId);
}
