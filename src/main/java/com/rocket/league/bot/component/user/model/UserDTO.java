package com.rocket.league.bot.component.user.model;

import basic.util.user.BotUser;
import com.rocket.league.model.rank.Rank;
import com.rocket.league.model.scraping.Stats;
import com.rocket.league.model.stats.StatsList;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO extends BotUser {

    private StatsList statsList;
    private List<Rank> rankList;
    private String platForm;
    private String platUser;

}
