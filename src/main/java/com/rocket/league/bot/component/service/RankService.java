package com.rocket.league.bot.component.service;

import static com.rocket.league.bot.constants.Constants.UNRANKED;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.rocket.league.bot.constants.RomanNumeral;
import com.rocket.league.model.rank.Rank;
import com.rocket.league.model.scraping.InitialData;
import com.rocket.league.model.scraping.MyData;
import com.rocket.league.model.scraping.Segments;

import reactor.core.publisher.Mono;

@Service
public class RankService {

	private final Logger logger = LoggerFactory.getLogger(RankService.class);

	private static final Predicate<Segments> isRanked = s -> s.getMetadata().getName().startsWith("Ranked");

	private static final Predicate<String> isSupersonic = "SUPERSONIC LEGEND"::equalsIgnoreCase;
	private static final Predicate<String> isGrandChampion = "GRAND CHAMPION"::equalsIgnoreCase;
	private static final Predicate<String> is2Words = s -> s.split(" ").length > 2;
	private static final Predicate<String> isUnranked = UNRANKED::equalsIgnoreCase;

	public Mono<List<Rank>> setRank(final MyData data) {
		final InitialData id = data.getData();

		return Mono
				.justOrEmpty(id.getSegments())
				.filter(l -> !l.isEmpty())
				.map(el -> el.stream().filter(isRanked).map(this::buildRank).collect(Collectors.toList()))
				.switchIfEmpty(Mono.defer(Mono::empty));
	}

	private Rank buildRank(final Segments s) {
		final String rank = getRank.apply(s.getStats().getTier().getMetadata().getName());
		final int level = getLevel.applyAsInt(s.getStats().getTier().getMetadata().getName());
		final String division = s.getStats().getDivision().getMetadata().getName();
		final String winStreak = s.getStats().getWinStreak().getDisplayValue();

		return Rank.builder().rank(rank).level(level).division(division).winStreak(winStreak).build();
	}

	private final UnaryOperator<String> getRank = s -> (is2Words.test(s) || isSupersonic.test(s))
			? s.split(" ")[0] + " " + s.split(" ")[1]
			: s.split(" ")[0];

	private final ToIntFunction<String> getLevel = s -> {
		if ((!isSupersonic.test(s) || !isGrandChampion.test(s)) && !isUnranked.test(s)) {
			if (is2Words.test(s) && !isGrandChampion.test(s))
				return RomanNumeral.valueOf(s.split(" ")[2]).toInt();
			else
				return RomanNumeral.valueOf(s.split(" ")[1]).toInt();
		}

		return 0;
	};

}
