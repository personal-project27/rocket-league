package com.rocket.league.bot.component.general.state.repository;

import org.com.bot.model.state.State;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface StateRepository extends ReactiveMongoRepository<State, String> {
    Mono<State> findByIsInitialStateAndIsAdminState(final Boolean isInitial, final Boolean isAdminState);
}
