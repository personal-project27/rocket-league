package com.rocket.league.bot.component.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;

import com.rocket.league.model.scraping.InitialData;
import com.rocket.league.model.scraping.MyData;
import com.rocket.league.model.scraping.Segments;
import com.rocket.league.model.scraping.Stats;
import com.rocket.league.model.stats.SingleStats;
import com.rocket.league.model.stats.StatsList;

import reactor.core.publisher.Mono;

@Service
public class StatsService{


	public Mono<StatsList> setStats(final MyData data) {
		final InitialData id = data.getData();

		return Mono
				.justOrEmpty(id.getSegments())
				.filter(l -> !l.isEmpty())
				.map(l -> l.get(0))
				.map(Segments::getStats)
				.flatMap(this::buildStats)
				.switchIfEmpty(Mono.defer(Mono::empty));
	}

	private Mono<StatsList> buildStats(final Stats s) {
		return Mono
				.just(StatsList
						.builder()
						.stats(List
								.of(SingleStats
										.builder()
										.rank(s.getAssists().getRank())
										.displayName(s.getAssists().getDisplayName())
										.displayValue(s.getAssists().getDisplayValue())
										.percentile(s.getAssists().getPercentile())
										.build(),
										SingleStats
												.builder()
												.rank(s.getGoals().getRank())
												.displayName(s.getGoals().getDisplayName())
												.displayValue(s.getGoals().getDisplayValue())
												.percentile(s.getGoals().getPercentile())
												.build(),
										SingleStats
												.builder()
												.rank(s.getGoalShotRatio().getRank())
												.displayName(s.getGoalShotRatio().getDisplayName())
												.displayValue(s.getGoalShotRatio().getDisplayValue())
												.percentile(s.getGoalShotRatio().getPercentile())
												.build(),
										SingleStats
												.builder()
												.rank(s.getmVPs().getRank())
												.displayName(s.getmVPs().getDisplayName())
												.displayValue(s.getmVPs().getDisplayValue())
												.percentile(s.getmVPs().getPercentile())
												.build(),
										SingleStats
												.builder()
												.rank(s.getSaves().getRank())
												.displayName(s.getSaves().getDisplayName())
												.displayValue(s.getSaves().getDisplayValue())
												.percentile(s.getSaves().getPercentile())
												.build(),
										SingleStats
												.builder()
												.rank(s.getScore().getRank())
												.displayName(s.getScore().getDisplayName())
												.displayValue(s.getScore().getDisplayValue())
												.percentile(s.getScore().getPercentile())
												.build(),
										SingleStats
												.builder()
												.rank(s.getShots().getRank())
												.displayName(s.getShots().getDisplayName())
												.displayValue(s.getShots().getDisplayValue())
												.percentile(s.getShots().getPercentile())
												.build(),
										SingleStats
												.builder()
												.rank(s.getWins().getRank())
												.displayName(s.getWins().getDisplayName())
												.displayValue(s.getWins().getDisplayValue())
												.percentile(s.getWins().getPercentile())
												.build()))
						.lastUpdate(LocalDateTime.now())
						.build());
	}

}
