package com.rocket.league.bot.component.service;

import static com.rocket.league.bot.constants.Constants.URL_API;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.rocket.league.bot.component.user.model.UserDTO;
import com.rocket.league.model.scraping.MyData;

import io.vavr.control.Try;
import reactor.core.publisher.Mono;

@Service
public class RocketLeagueServiceImpl implements RocketLeagueService {

	private final RestTemplate restTemplate = new RestTemplate();
	private final Logger logger = LoggerFactory.getLogger(RocketLeagueServiceImpl.class);

	public final Mono<MyData> retrieveStats(final UserDTO u) {
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		final String url = URL_API + u.getPlatForm() + "/" + u.getPlatUser();

		return Mono
				.justOrEmpty(Try
						.of(() -> restTemplate.getForObject(url, MyData.class))
						.onFailure(l -> logger.error("Errore nell'esecuzione della chiamata", l))
						.getOrNull());

	}
}