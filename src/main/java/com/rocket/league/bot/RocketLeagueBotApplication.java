package com.rocket.league.bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@EnableReactiveMongoRepositories
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class RocketLeagueBotApplication {
    public static void main(String[] args) {
        SpringApplication.run(RocketLeagueBotApplication.class, args);
    }
}
