package com.rocket.league.bot.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public static final String URL_API = "https://api.tracker.gg/api/v2/rocket-league/standard/profile/";

    public static final String UNRANKED = "UnRanked";

}