package com.rocket.league.bot.constants;

import java.util.Arrays;

public enum RomanNumeral {
    I(1), II(2), III(3), UNRAKED(0);

    public final int value;

    private RomanNumeral(int value) {
        this.value = value;
    }

    public int toInt() {
        return value;
    }

    public static RomanNumeral resolve(final int value) {
        return Arrays
                .stream(values())
                .filter(s -> s.value == value)
                .findFirst()
                .orElse(UNRAKED);
    }


}
